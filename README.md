# parse-server-vuejs-todo

## launch mongodb :
``` .\3.6\bin\mongod.exe --dbpath E:\Mongodb\testDatabase ```
*(or adapt the dbpath to your system)*

## launch server :
``` npm start ```
*(alias for a "node index.js")*

## launch vuejs client :
``` php -S localhost:3100 ```
*(launch a web server for ajax requests)*